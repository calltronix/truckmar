
        <footer class="footer footer-alt">
            2016 - 2019 &copy; Minton theme by <a href="#" class="text-muted">Coderthemes</a> 
        </footer>

        <!-- Vendor js -->
        <script src="<?php echo base_url() ?>assets/js/vendor.min.js"></script>

        <!-- App js -->
        <script src="<?php echo base_url() ?>assets/js/app.min.js"></script>
        
    </body>
</html>